terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "web" {
  ami           = "ami-0bcf5425cdc1d8a85"
  instance_type = "t2.micro"
  key_name      = "lqs-key"

  tags = {
    Name = "HelloWorld-TF"
  }
}


resource "aws_key_pair" "deployer" {
  key_name   = "lqs-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDBgAf03/7KeT6fLiTOWf7YQvjcEDa0OjICRhrmLpr2+Q+E2JYHTU/dObt9otRi0sF+hO37aTAK7zFzutBCYwVTJdel2B2IitUWJg5rak1f9ZzANmAgTON3xtbJpMkCDMz8g2ZT6hv6GJKrFxtPv/1Eoyl6JZ25hYC/8iZDlKGe6onspE6yBAHUPZuAmmCjORZtuZ7JEUg7L5EaunZxdWlCiZkTMm+T+Mv6I30EdPKjDoWoitx4vcBYj+xcwATkz60p14+AM2XYXbNrEebRgfSwoPBE29b+teReJSPADRb9bUxD/4wH9UCjWTiTiFiX8oxxxQOBj7FwO09LWmrDHGYBjv+Wz17AXb+SppwgZg8peSoLPg0euDhxULBoh+nLbJWwr28vLFDUeYuKFloaGHOepN8l8kiVo8g3Ov+gKm2lvfCP3LAbLZUSnhvJ6ghSJYwP1hnD2ygao2l91zqdIrpac+Nh6ppp90E4hk9uGKLBdXuxn6qGlPtWf/pLAVnAsY8= Devashish Kureel@Dev-Office-PC"
}